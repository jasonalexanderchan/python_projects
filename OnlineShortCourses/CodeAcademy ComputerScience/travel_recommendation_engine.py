destinations = ['Paris, France', 'Shanghai, China', 'Los Angeles, USA', 'Sao Paulo, Brazil', 'Cairo, Egypt']
test_traveler = ['Erin Wilkes', 'Shanghai, China', ['historical site', 'art']]

attractions = []
idx = len(destinations)
for i in range(idx):
    attractions.append([])

def get_destination_index(destination):
    destination_index = destinations.index(destination)
    return destination_index


def get_traveler_location(traveler):
    traveler_destination = test_traveler[1]
    traveler_destination_index = destinations.index(traveler_destination)
    return traveler_destination_index

# debug
# print(get_traveler_location(test_traveler))
# output = get_destination_index(“Hyderabad, India")

def add_attraction(destination, attraction):
    try:
        destination_index = get_destination_index(destination)
        attractions[destination_index].append(attraction)
        return attractions
    except ValueError:
        print("no destination")
        return

add_attraction('Los Angeles, USA', ['Venice Beach', ['beach']])
add_attraction("Paris, France", ["the Louvre", ["art", "museum"]])
add_attraction("Paris, France", ["Arc de Triomphe", ["historical site", "monument"]])
add_attraction("Shanghai, China", ["Yu Garden", ["garden", "historical site"]])
add_attraction("Shanghai, China", ["Yuz Museum", ["art", "museum"]])
add_attraction("Shanghai, China", ["Oriental Pearl Tower", ["skyscraper", "viewing deck"]])
add_attraction("Los Angeles, USA", ["LACMA", ["art", "museum"]])
add_attraction("São Paulo, Brazil", ["São Paulo Zoo", ["zoo"]])
add_attraction("São Paulo, Brazil", ["Pátio do Colégio", ["historical site"]])
add_attraction("Cairo, Egypt", ["Pyramids of Giza", ["monument", "historical site"]])
add_attraction("Cairo, Egypt", ["Egyptian Museum", ["museum"]])


# destination_index = get_destination_index('Cairo, Egypt')
# all_attractions = attractions[destination_index]
# print(all_attractions)

def find_attractions(destination, interests):
    destination_index = get_destination_index(destination)
    all_attractions = attractions[destination_index]

    possible_attraction = []


    # for each attraction in this city
    for attract in all_attractions:

        # what are the tags for this specific attraction?
        current_tag = attract[1]

        for tag in current_tag:
            # does that match each interest in my interest list?
            for interest in interests:
                if tag == interest:
                    possible_attraction.append(attract[0])

    return possible_attraction

# Test
result1  = find_attractions('Paris, France', ['monument', 'art'])
print(result1)

def get_attractions_for_traveler(traveler):
    traveler_name = traveler[0]
    traveler_destination = traveler[1]
    traveler_interests = traveler[2]
    traveler_attractions = find_attractions(traveler_destination, traveler_interests)
    print("Hi "+
          traveler_name +
          " we think you will like these places around " +
          traveler_destination +
          ": " +
          ", ".join(traveler_attractions))


result2 = get_attractions_for_traveler(['Dereck Smill', 'Paris, France', ['monument', 'art']])
