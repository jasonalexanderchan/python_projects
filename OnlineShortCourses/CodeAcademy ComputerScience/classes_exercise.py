class Student:
    def __init__(self, name, year):
        self.name = name
        self.year = year
        self.grades = []

    def add_grade(self, grade):
        if type(grade) == type(Grade(grade)):
            return self.grades.append(grade.score)
        else:
            return None

class Grade:
    minimum_passing = 65

    def __init__(self, score):
        self.score = score

# Test Grade
# test = Grade(88)
# print(test.score)
# 88

# Test Student
roger = Student('Roger van der Weyden', 10)
sandro = Student('Sandro Botticelli', 12)
pieter = Student('Pieter Bruegel the Elder', 8)

# Test object and method with another Class as parameter
# roger.add_grade(Grade(100))
# roger.add_grade(Grade(99))
# result = roger.grades
# print(result)
# [100, 99]

pieter.add_grade(Grade(100))
print(pieter.grades)