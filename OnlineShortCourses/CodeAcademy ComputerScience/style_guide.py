""" This file is are my notes on Python style guide

Write docstrings for all public modules, functions, classes and methods.
Docstrings are not necessary for non-public methods.
"""
# Source: https://www.python.org/dev/peps/pep-0008/#introduction

# Commenting
# Always start with a capital letter.
# Full sentences.
# End with full stop.
# Use inline comments sparingly because it is distracting if they sate the obvious.

# Naming conventions
# Single trailing underscore class_ is used to differentiate from python variables.
# Never use l, O, I as single character variable names.
# Error suffix should be used for Exception Names e.g. user_input_error.
# Function names should be lowercase with words separated by underscores as necessary.
# CONSTANTS are always in capitals with underscores.


# Spaces or tab?
# 4x spaces are preferred to tab. Python can tell the difference.

# Max line length = 79 chars
# Break math like this
# Income = (gross_wages
#           + taxable interest
#           + dividends
#           - interest)

# Blank lines
# Surround top level function and class definitions with 2x blank lines
# Use blank lines sparingly in functions


# Imports
# Should always be on separate lines. Always the top of the file. After any docstrings
# import os
# import sys

# Quotes
# No recommendation. Choose one and be consistent.

# Whitespace
# Avoid extra whitespaces. Good practice examples. Dont try to align operators!
# NEVER have more than one space.
# ALWAYS have the same amount of whitespace on bothsides of a binary operators
# foo = (0,) not foo = (0, )