# This project practises my string manipulation

import re

# These are the emails you will be censoring. The open() function is opening the text file that the emails are contained in and the .read() method is allowing us to save their contexts to the following variables:
email_one = open("email_one.txt", "r").read()
email_two = open("email_two.txt", "r").read()
email_three = open("email_three.txt", "r").read()
email_four = open("email_four.txt", "r").read()

proprietary_terms = ["she", "personality matrix", "sense of self", "self-preservation", "learning algorithm", "her", "herself"]
# Sort by descending string length because edge case in censor loop: danger and dangerous
proprietary_terms.sort(key=len, reverse=True)

negative_words = ["concerned", "behind", "danger", "dangerous", "alarming", "alarmed", "out of control", "help", "unhappy", "bad", "upset", "awful", "broken", "damage", "damaging", "dismal", "distressed", "distressed", "concerning", "horrible", "horribly", "questionable"]
negative_words.sort(key=len, reverse=True)

# This function censors the proprietary terms and negative words list
# Input is a string
# Output is a string
def censor_text(text):
    output = text

    for word in proprietary_terms:
        output = output.replace(word,'<REDACTED>')
    for word in negative_words:
        output = output.replace(word,'<REDACTED>')
    return output

# This function censors not only the words in the proprietary terms and negative words list, but also the word preceding and following that
# Input is a string
# Output is a string
def censor_text_extreme(text):
    output = text

    for word in proprietary_terms:
        output = output.replace(word, '<REDACTED>')

    for word in negative_words:
        output = output.replace(word, '<REDACTED>')

    output_split = output.split(' ')
    for idx, word in enumerate(output_split, start=0):
        if word == '<REDACTED>':
            output_split[idx - 1] = '<REDACTED-1>'
            output_split[idx + 1] = '<REDACTED+1>'
    output_join = ' '.join(output_split)

    return output_join

# Test case 1: email_one
email_one_censored = censor_text(email_one)
# print(email_one)
# print(email_one_censored)

# Test case 2: email_two
email_two_censored = censor_text(email_two)
# print(email_two)
# print(email_two_censored)

# Test case 3: email_three
email_three_censored = censor_text(email_three)
# print(email_three)
# print(email_three_censored)

# Test case 4: email_four
email_four_censored = censor_text(email_four)
print(email_four)
print(email_four_censored)

# Test case 4 extreme: email_four
email_four_censored_extreme = censor_text_extreme(email_four)
print(email_four)
print(email_four_censored_extreme)
