# This is an exercise about modules

# A workspace is called scope in python.
# A file cannot access the scope of another file even if it is in the same directory.
# Files are actually modules!
# from filename import function name

# Q. Are we limited to importing from the current directory?
# No. You can import from the subdirectories of a directory at the same level as your file.
# from directory.filename import function_name


# To see all the objects and functions in a module.
# print(dir(module)).
print('Starting ')
from datetime import datetime
current_time = datetime.now()
print(current_time)
print(dir(datetime))

# Name spaces describe your local workspace (in matlabspeak) vs. that of other modules.
# You can 'alias' imported names spaces e.g. import tensorflow as tf.

from matplotlib import pyplot as plt
import random

# Create list 1 to 12 inclusive.
numbers_a = list(range(1,13))
print(numbers_a)

numbers_b = random.sample(range(1000),12)
print(numbers_b)

plt.plot(numbers_a, numbers_b)
# plt.show()


# Python precision needs to be controlled explicitly.

from decimal import Decimal

a = 0.123123
b = Decimal(a)
print(b)

c = Decimal('0.0123123')
print(c)

d = 0.1 + 0.2
print(d)

e = Decimal('0.1') + Decimal('0.2')
print(e)