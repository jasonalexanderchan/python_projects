'''
You’ve started position as the lead programmer for the family-style Italian restaurant Basta Fazoolin’ with My Heart. The restaurant has been doing fantastically and seen a lot of growth lately. You’ve been hired to keep things organized.
'''

class Menu:
    def __init__(self, name, items, start_time, end_time):
        self.name = name
        self.items = items
        self.start_time = start_time
        self.end_time = end_time
        print('This is the {d} menu.'.format(d=self.name))

    def __str__(self):
        return "The {name} menu is available from {start_time} to {end_time}.".format(name= self.name, start_time = self.start_time, end_time = self.end_time)

    def calculate_bill(self, purchased_items):
        # Sum the cost of items in the list
        bill = 0
        for item in purchased_items:
            bill += self.items.get(item, 0)
        return bill

# Define menus
brunch_items = {'pancakes': 7.50, 'waffles': 9.00, 'burger': 11.00, 'home fries': 4.50, 'coffee': 1.50, 'espresso': 3.00, 'tea': 1.00, 'mimosa': 10.50, 'orange juice': 3.50}
brunch_start_time = 1100
brunch_end_time = 1600

early_bird_items = {'salumeria plate': 8.00, 'salad and breadsticks (serves 2, no refills)': 14.00, 'pizza with quattro formaggi': 9.00, 'duck ragu': 17.50, 'mushroom ravioli (vegan)': 13.50, 'coffee': 1.50, 'espresso': 3.00}
early_bird_start_time = 1500
early_bird_end_time = 1800

dinner_items ={'crostini with eggplant caponata': 13.00, 'ceaser salad': 16.00, 'pizza with quattro formaggi': 11.00, 'duck ragu': 19.50, 'mushroom ravioli (vegan)': 13.50, 'coffee': 2.00, 'espresso': 3.00,}
dinner_start_time = 1700
dinner_end_time = 2300

kids_items = {'chicken nuggets': 6.50, 'fusilli with wild mushrooms': 12.00, 'apple juice': 3.00}
kids_start_time = 1100
kids_end_time = 2100

# Create menus
early_bird = Menu('early_bird', early_bird_items, early_bird_start_time, early_bird_end_time)
brunch = Menu('brunch', brunch_items, brunch_start_time, brunch_end_time)
dinner = Menu('dinner', dinner_items, dinner_start_time, dinner_end_time)
kids = Menu('kids', kids_items, kids_start_time, kids_end_time)

# Test the string representation method
print(early_bird)
print(brunch)
print(dinner)
print(kids)

# Test the calculate_bill method
brunch_bill = brunch.calculate_bill(['pancakes', 'home fries', 'coffee'])
early_bird_bill = early_bird.calculate_bill(['salumeria plate', 'mushroom ravioli (vegan)'])
print(brunch_bill, early_bird_bill)

'''Basta Fazoolin’ with my Heart has seen tremendous success with the family market, which is fantastic because when you’re at Basta Fazoolin’ with my Heart with family, that’s great! We’ve decided to create more than one restaurant to offer our fantastic menus, services, and ambience around the country.'''

class Franchise:
    def __init__(self, address, menu):
        # Menu must be a list.
        self.address = address
        self.menu = menu

    def __str__(self):
        return "This franchise is located at {address}.".format(address = self.address)

    def available_menus(self, time):
        available_menus = []
        # Time variable must be in 24hr format as 4 digits.
        # Menu times overlap.
        if brunch.start_time <= time <= brunch.end_time:
            available_menus.append('brunch')
        if early_bird.start_time <= time <= early_bird.end_time:
            available_menus.append('early_bird')
        if dinner.start_time <= time <= dinner.end_time:
            available_menus.append('dinner')
        if kids.start_time <= time <= kids.end_time:
            available_menus.append('kids')
        return available_menus

# Create the franchises
flagship_store = Franchise('1232 West End Road', [early_bird, brunch, dinner, kids])
second_store = Franchise('12 East Mulberry Street', [early_bird, brunch, dinner, kids])
print(flagship_store, second_store)

# Test available menus
print(flagship_store.available_menus(1700))

'''Since we’ve been so successful building out a branded chain of restaurants, we’ve decided to diversify. We’re going to create a restaurant that sells arepas!'''

class Business:
    def __init__(self, name, franchises):
        self.name = name
        self.franchises = franchises

arepas_items = {'arepa pabellon': 7.00, 'pernil arepa': 8.50, 'guayanes arepa': 8.00, 'jamon arepa': 7.50}
arepas_start_time = 1000
arepas_end_time = 2000
arepas_menu = Menu('arepa', arepas_items, arepas_start_time, arepas_end_time)
arepas_place = Franchise('189 Fitzgerald Avenue', ['arepas_menu'])

my_business = Business('Take a\' Arepa', ['flagship_store', 'second_store', 'arepas_place'])
print(my_business.name, my_business.franchises)
