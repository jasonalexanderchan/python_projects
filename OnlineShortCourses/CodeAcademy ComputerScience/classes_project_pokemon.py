class Pokemon:
    def __init__(self, name, level, type, max_hp, current_hp, knocked_out_flag):
        self.name = name
        self.level = level
        self.type = type
        self.max_hp = max_hp
        self.current_hp = current_hp
        self.knocked_out_flag = knocked_out_flag

    def change_health(self, delta_hp):
        # Lose health from pokemon attack
        if delta_hp < 0:
            print('{pokemon} was hurt {this_much} health.'.format(pokemon=self.name, this_much=delta_hp))
        # Gain health from potion
        if delta_hp > 0:
            if delta_hp + self.current_hp > self.max_hp:
                delta_hp = self.max_hp - self.current_hp
            print('{pokemon} gained {this_much} health!'.format(pokemon=self.name, this_much=delta_hp))
        # Calculate remaining health
        self.current_hp = self.current_hp + delta_hp
        if self.current_hp > 0:
            print('{pokemon} has {this_much} health left.'.format(pokemon=self.name, this_much=self.current_hp))
        else:
            self.current_hp = 0
            self.knocked_out_flag = True
            print('{pokemon} now has 0 health.'.format(pokemon=self.name))
            print('{pokemon} has fainted!'.format(pokemon=self.name))

    def revive(self):
        self.current_hp = self.max_hp
        self.knocked_out_flag = False
        print('{pokemon} is revived back to full health!'.format(pokemon=self.name))

    # Check if pokemon is already knocked out
    def attacks(self, other_pokemon, attack_damage):
        # Are the pokemon even alive?
        if self.knocked_out_flag == True:
            print('{pokemon} cannot attack because it is already dead.'.format(pokemon=self.name))
            return
        if other_pokemon.knocked_out_flag == True:
            print('{pokemon} cannot be attacked because it is already dead.'.format(pokemon=other_pokemon.name))
            return

        # other_pokemon is a Pokemon Object
        print('{pokemon} attacks {other_pokemon}!'.format(pokemon=self.name, other_pokemon=other_pokemon.name))

        if self.type =='fire' and other_pokemon.type == 'grass':
            attack_damage = 2 * attack_damage
            super_effective_flag = True
            not_effective_flag = False
        if self.type =='grass' and other_pokemon.type ==  'fire':
            attack_damage = 0.5 * attack_damage
            super_effective_flag = False
            not_effective_flag = True
        if self.type =='water' and other_pokemon.type == 'fire':
            attack_damage = 2 * attack_damage
            super_effective_flag = True
            not_effective_flag = False
        if self.type =='fire' and other_pokemon.type == 'water':
            attack_damage = 0.5 * attack_damage
            super_effective_flag = False
            not_effective_flag = True
        if self.type =='grass' and other_pokemon.type == 'water':
            attack_damage = 2 * attack_damage
            super_effective_flag = True
            not_effective_flag = False
        if self.type =='water' and other_pokemon.type == 'grass':
            attack_damage = 0.5 * attack_damage
            super_effective_flag = False
            not_effective_flag = True

        # Update hp of other_pokemon
        other_pokemon.change_health(attack_damage)
        if super_effective_flag == True:
            print('{pokemon}\'s attack was super effective!'.format(pokemon = self.name))
        if not_effective_flag == True:
            print('{pokemon}\'s attack was not very effective...'.format(pokemon=self.name))

        print('\n')

class Trainer:
    def __init__(self, name, pokemon_list, num_potions, pokemon_idx):
        # pokemon_list is a list of pokemon objects
        self.name = name
        self.pokemon_list = pokemon_list
        self.num_potions = num_potions
        self.pokemon_idx = pokemon_idx

    def use_potion(self):
        if self.num_potions != 0:
            print('You used a potion!')
            self.pokemon_list[self.pokemon_idx].change_health(30)
            self.num_potions += -1
            print('You have {this_many} potions left.'.format(this_many=self.num_potions))
        else:
            print('You don\'t have any potions.')

        print('\n')

    def choose_pokemon(self, desired_pokemon_idx):
        # Index must be 0-5
        if self.pokemon_list[desired_pokemon_idx].knocked_out_flag == True:
            print('{pokemon} is dead. Choose another pokemon!'.format(pokemon=self.pokemon_list[desired_pokemon_idx].name))
        else:
            self.pokemon_idx = desired_pokemon_idx
            print('{trainer} chooses {pokemon} to enter the battle.'.format(trainer=self.name, pokemon=self.pokemon_list[self.pokemon_idx].name))

        print('\n')

# Set up me
my_charmander = Pokemon('charmander', 21, 'fire', 100, 70, True)
my_bulbasaur = Pokemon('bulbasaur', 21, 'grass', 100, 50, False)
my_squirtle = Pokemon('squirtle', 21, 'water', 100, 70, False)
my_charizard = Pokemon('charizard', 21, 'fire', 100, 70, False)
my_caterpie = Pokemon('caterpie', 5, 'grass', 50, 50, False)
my_seahorse = Pokemon('seahorse', 5, 'water', 40, 40, False)
my_pokemons = [my_charmander, my_bulbasaur, my_squirtle, my_charizard, my_caterpie, my_seahorse]
my_potions = 0
my_idx = 1
my_name = 'Ash'
me = Trainer(my_name, my_pokemons, my_potions, my_idx)

# Set up you
your_squirtle = Pokemon('Squirtle_evil', 22, 'water', 120, 90, False)
your_charmander = Pokemon('Charmander_evil', 21, 'fire', 100, 70, False)
your_bulbasaur = Pokemon('Bulbasaur_evil', 21, 'grass', 100, 70, False)
your_charizard = Pokemon('Charizard_evil', 21, 'fire', 100, 70, False)
your_caterpie = Pokemon('Caterpie_evil', 5, 'grass', 50, 50, False)
your_seahorse = Pokemon('Seahorse_evil', 5, 'water', 40, 40, False)
your_pokemons = [your_squirtle, your_charmander, your_bulbasaur, your_charizard, your_caterpie, your_seahorse]
your_potions = 3
your_idx = 1
your_name = 'Oak'
you = Trainer(your_name, your_pokemons, your_potions, your_idx)

# Battle
me.choose_pokemon(1)
you.choose_pokemon(1)

me.pokemon_list[my_idx].attacks(you.pokemon_list[your_idx], -20)
you.pokemon_list[your_idx].attacks(me.pokemon_list[your_idx], -20)
me.use_potion()
you.pokemon_list[your_idx].attacks(me.pokemon_list[your_idx], -20)

# Test case: dead pokemon cannot attack or be attacked
me.pokemon_list[my_idx].attacks(you.pokemon_list[your_idx], -20)
you.pokemon_list[your_idx].attacks(me.pokemon_list[your_idx], -20)

# IMPROVEMENTS / LESSONS LEARNT
# More logic breakdowns
# charmanders, bulbasaurs etc are subclasses of pokemon. Use .super() to inherit properties of pokemon.
# damage is a property of the specific pokemon. Shouldn't be an parameter in Trainer class.
# square brackets indexing is confusing. Simplify to:  me.attack(you), you.attack(me)