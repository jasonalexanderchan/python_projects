# This is the dictionary exercise from Code Academy. Not bad. Good size.


available_items = {"health potion": 10, "cake of the cure": 5, "green elixir": 20, "strength sandwich": 25, "stamina grains": 15, "power stew": 30}
health_points = 20

# In one line, add the value of "stamina grains" to health_points and remove the item from the dictionary. If the key does not exist, add 0 to health_points.
_var = available_items.get("stamina grains", 0)
if _var != 0:
  health_points = health_points + available_items.get("stamina grains")
  available_items.pop('stamina grains')

print(available_items)
print(health_points)


# The trick is to know that pop returns a value and then deletes it.
# Then the above 4 lines of code can be condensed into 1 line.
health_points += available_items.pop("power stew", 0)