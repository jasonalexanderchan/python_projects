from stack import Stack

print("\nLet's play Towers of Hanoi!!")

# Create the Stacks
stacks = []
left_stack = Stack("Left")
right_stack = Stack("Right")
middle_stack = Stack("Middle")
stacks.append(left_stack)
stacks.append(middle_stack)
stacks.append(right_stack)

# Set up the Game
num_disks = int(input("Enter your number: "))
while num_disks < 3:
    num_disks = int(input("Bro, it has to be greater than 3: "))
print("Ok now we playing with {num}".format(num=num_disks))

# Create disk values, place on left stack
for i in range(num_disks, 0, -1):
    left_stack.push(i)

# Calculate the number of optimal moves
num_optimal_moves = 2**num_disks - 1
print("\nThe fastest you can solve this game is in {opt} moves".format(opt=num_optimal_moves))

# Get User Input
def get_input():
    # Prompt user to choose stack based on first letter
    choices = [stack.get_name()[0] for stack in stacks]
    while True:
        for i in range(len(stacks)):
            name = stacks[i].get_name()
            letter = choices[i]
            print('Enter {letter} for {name}'.format(letter=letter, name=name))

        user_input = input("")

        # Check user input is valid
        if user_input in choices:
            for i in range(len(stacks)):
                if user_input == choices[i]:
                    return stacks[i]

# Play the Game
num_user_moves = 0
while(right_stack.get_size() != num_disks):
    print("\n\n\n...Current Stacks...")
    for stack in stacks:
        stack.print_items()

    while True:
        print("\nWhich stack do you want to move from?\n")
        from_stack = get_input()
        print("\nWhich stack do you want to move to?\n")
        to_stack = get_input()

        # Check if moves are valid
        if from_stack.get_size() == 0:
            print("\n\nInvalid Move. Try Again")
        elif to_stack.get_size() == 0 or from_stack.peek() < to_stack.peek():
            disk = from_stack.pop()
            to_stack.push(disk)
            num_user_moves += 1
            break
        else:
            print("\n\nInvalid Move. Try Again")

print("\n\nYou completed the game in {num} moves, and the optimal number of moves is {num2}".format(num=num_user_moves, num2=num_optimal_moves))