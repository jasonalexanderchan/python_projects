import datetime

class Art:
    def __init__(self, artist, title, medium, year, owner):
        self.artist = artist
        self.title = title
        self.medium = medium
        self.year = year
        self.owner = owner

    def __str__(self):
        if self.owner.is_museum == False:
            building = self.owner.location + ', Private collection'
        else:
            building = self.owner.location
        return '{artist}. \'{title}\'. {year}, {medium}. {owner_name}, {building}.'.format(artist=self.artist, title=self.title, medium=self.medium, year=self.year, owner_name=self.owner.name, building=building)

class Marketplace:
    def __init__(self, listings):
        self.listings = listings

    def add_listings(self, new_listing):
        self.listings.append(new_listing)

    def remove_listings(self, undesired_listing):
        self.listings.remove(undesired_listing)

    def show_listings(self):
        if self.listings == []:
            print('There are no listings at the marketplace.')
        else:
            for item in self.listings:
                print('{this_listing} is now listed for sale at the marketplace.'.format(this_listing=item.art.title))

class Client:
    def __init__(self, name, location, is_museum, wallet_size):
        self.name = name
        self.location = location
        self.is_museum = is_museum
        self.wallet_size = wallet_size

    def sell_artwork(self, new_listing, market):
        # check that client owns art work
        if new_listing.art.owner.name == self.name:
            market.add_listings(new_listing)
            print('{client} is selling {art} for ${price}.'.format(client=self.name, art=new_listing.art.title,price=new_listing.price))

    def buy_artwork(self, artwork, market):
        print('{client} would like to purchase {art}'.format(client=self.name, art=artwork.title))

        # check client does not already own the artwork
        if artwork.owner.name != self.name:
            print('Facilitating the transaction...the client does not already own the art.')
            # check that the market has that art in its listings
            for listing in market.listings:
                if artwork == listing.art:
                    #Check that the client has enough money
                    if self.wallet_size > listing.price:
                        # Update wallet_size
                        self.wallet_size = self.wallet_size - listing.price
                        # Update the art with the new owner's name
                        print('Facilitating the transaction...the listing is at the marketplace.')
                        listing.art.owner.name = self.name
                        print('Facilitating the transaction...the transaction is approved!')
                        print('The new owner of {art} is now {owner}.'.format(art=artwork.title,owner=self.name))
                        print('The listing will now be removed from the marketplace.')
                        # Remove listing from the market
                        market.remove_listings(listing)
                    else:
                        print('Not enough money to complete the purchase. The transaction is cancelled.')
                else:
                    print('The market doesn\'t have that artwork.')
        else:
            print('You already own it.')

class Listing:
    def __init__(self, art, price, seller, expiration_date):
        self.art = art
        self.price = price
        self.seller = seller
        self.expiration_date = expiration_date

    def __str__(self):
        return '***NEW*** listing is created: {title_of_art}, ${price}.'.format(title_of_art=self.art.title, price=self.price)

    def days_left(self):
        # __days_left is a datetime object
        __days_left__ = self.expiration_date - datetime.date.today()
        if __days_left__.days <= 0:
            print(self.seller.show_listings())
            print('***WARNING***: The listing has expired and will be removed from the market.')
        else:
            print('There is {this_many} days left before the listing expires.'.format(this_many=__days_left__.days))

# Test marketplace class
veneer = Marketplace([])
# print(veneer.listings)
# veneer.show_listings()

edytta = Client('Edytta', 'Halprit', False, 50000)
moma = Client('MOMA', 'New York', True, 1000000)

# 1. Create an Art Object
girl_with_mandolin = Art('Picasso, Pablo','Girl with a Mandolin (Fanny Tellier)','oil on canvas', 1910, moma)

# 2. Create an Art listing
art_listing_a = Listing(girl_with_mandolin, 10000, veneer, datetime.date(2020,10,10))
print(art_listing_a)
art_listing_a.days_left()

# 3. Put the Art listing on a Market
moma.sell_artwork(art_listing_a, veneer)
veneer.show_listings()

# 4. Facilitate buying of that listing
edytta.buy_artwork(girl_with_mandolin, veneer)
veneer.show_listings()

#5. Check that the transaction worked as expected
# Expect new owner to be Edytta
# print(girl_with_mandolin.owner.name)
# Expect Edytta's wallet to go down by the price of the artwork
# print(edytta.wallet_size)

# TODO
# create a wishlist for clients