# -*- coding: utf-8 -*-
"""
Created on Sun Mar 24 16:07:45 2019

@author: jason
"""
# Convention: classes need to have a Capital first letter
# Convention: class variables need to be ALL CAPS
# Self is not a python keyword. It can be set to anything.

import webbrowser

class Movie():
    # Documentation associated with the class 'Movie'
    """ This class provides a way to store movie related information """
    
    # Class variables
    VALID_RATINGS = ["G", "PG", "PG-13", "R" ]
    
    # Constructor
    def __init__(instance, movie_title, movie_storyline, poster_image, 
                 trailer_youtube):
        instance.title = movie_title
        instance.storyline = movie_storyline
        instance.poster_image_url = poster_image
        instance.trailer_youtube_url = trailer_youtube
    
    # Class method
    def show_trailer(instance):
        webbrowser.open(instance.trailer_youtube_url)
        