import os #operating system functions

def rename_files(): #define function
    file_list = os.listdir(r"C:\Users\jason\Downloads\prank\prank") #r = rawpack. Interpret literally.
    print(file_list)
    
    table = str.maketrans(dict.fromkeys('0123456789')) #https://stackoverflow.com/questions/41708770/translate-function-in-python-3

    #os.getcwd()
    os.chdir(r"C:\Users\jason\Downloads\prank\prank")
        
    for file_name in file_list:
        print("before " +file_name)
        os.rename(file_name,file_name.translate(table))        
        print("after  " +file_name)     

rename_files() #run function
