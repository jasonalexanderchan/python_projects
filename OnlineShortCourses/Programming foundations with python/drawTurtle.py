import turtle

def draw_square():
    window = turtle.Screen()
    window.bgcolor("red")
    
    brad = turtle.Turtle()
    brad.shape("turtle"); brad.color("yellow"); brad.speed(2)

    for i in range(4):
        brad.forward(100)
        brad.right(90)

    sam = turtle.Turtle()
    sam.shape("turtle"); sam.color("blue"); sam.speed(2)
    sam.circle(100)

    chris = turtle.Turtle()
    chris.shape("turtle"); chris.color("green"); chris.speed(2)

    for i in range(2):
        chris.forward(100)
        chris.left(120)        
    chris.forward(100)
    
    window.exitonclick()

draw_square()
