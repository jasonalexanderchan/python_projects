# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
import urllib

def read_text():
#    Alternate way of opening files
#    path = r"C:\Users\jason\OneDrive\Documents\Python Scripts\profanityText.txt"
#    quotes = open(path,'r')
    
    quotes = open(r"C:\Users\jason\OneDrive\Documents\Python Scripts\profanityText.txt")    
    contents_of_file = quotes.read()
    print(contents_of_file)
    quotes.close()
    check_profanity(contents_of_file)

def check_profanity(text_to_check):
    with urllib.request.urlopen("http://www.wdylike.appspot.com/?q=" + text_to_check) as response:
        html = response.read()
    print(html)
    
#    Udacity's way of url request as functions
#    connection = urllib.request.urlopen("http://www.wdylike.appspot.com/?q="+text_to_check)
#    output = response.read()
#    print(output)
#    connection.close()
    
read_text()