# -*- coding: utf-8 -*-
"""
Created on Sat Mar 23 23:43:57 2019

@author: jason
"""
import twilio

# Download the helper library from https://www.twilio.com/docs/python/install
# https://github.com/twilio/twilio-python/blob/master/twilio/rest/__init__.py
# Understand git directory structure:twilio/rest. Peek into __init__.py
# This is where the classes are defined.
from twilio import rest 


# Your Account Sid and Auth Token from twilio.com/console
# DANGER! This is insecure. See http://twil.io/secure
account_sid = 'ACaee4e384c48b44e40500e5427854245e'
auth_token = 'f9669c0d15ca3089677e81ac380c2b24'  
client = Client(account_sid, auth_token) # Define object using class Client

message = client.messages \
                .create(
                     body="Join Earth's mightiest heroes. Like Kevin Bacon.",
                     from_='+61429210735',
                     to='+61402799892'  # Deleted to preven security leak
                 )

print(message.sid)