# -*- coding: utf-8 -*-
"""
Created on Sun Mar 24 18:17:30 2019

@author: jason
"""

class Parent():
    def __init__(self, last_name, eye_colour):
        print("Parent constructor called")
        self.last_name = last_name
        self.eye_colour = eye_colour
        
    def show_info(self):
        print("Last name is " + self.last_name)
        print("eye colour is " + self.eye_colour)
    
# Inherit everything that is publicly available by Parent
class Child(Parent):
    def __init__(self, last_name, eye_colour, number_of_toys):
        print("Child constructor called")
        Parent.__init__(self, last_name, eye_colour)
        self.number_of_toys = number_of_toys
        
    def show_info(self):
        print("Last name is " + self.last_name)
        print("Eye colour is " + self.eye_colour)
        print("Number of toys is " + str(self.number_of_toys))
