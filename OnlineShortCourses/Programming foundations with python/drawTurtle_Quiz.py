import turtle

def draw_square(someTurtle):
    for i in range(4):
        someTurtle.forward(100)
        someTurtle.right(90)

def draw_all():
    window = turtle.Screen()
    window.bgcolor("red")

    #Create turtle, Brad
    brad = turtle.Turtle()
    brad.shape("turtle"); brad.color("yellow"); brad.speed(100)

    for i in range(36):
        brad.left(10)
        draw_square(brad)

    window.exitonclick()

draw_all()
