# -*- coding: utf-8 -*-
"""
Created on Sun Mar 24 16:09:12 2019

@author: jason
"""

import media
import freshTomatoes

# Create instance Toy Story from Class 'Movie' in Module 'media'
toy_story = media.Movie("Toy Story",
                        "Toys come to life",
                        "https://upload.wikimedia.org/wikipedia/en/1/13/Toy_Story.jpg",
                        "https://www.youtube.com/watch?v=KYz2wyBy3kc")

# Debug: print(toy_story.storyline)

# Create instance Avatar from Class 'Movie' in Module 'media'
avatar = media.Movie("Avatar",
                     "Pocahontas with aliens",
                     "https://upload.wikimedia.org/wikipedia/en/b/b0/Avatar-Teaser-Poster.jpg",
                     "https://www.youtube.com/watch?v=6ziBFh3V1aM")

# Debug: toy_story.show_trailer()
# Note: Instance methods need an () to work

# Create instance Bladerunner 2049 from Class 'Movie' in Module 'media'
bladerunner2049 = media.Movie("Bladerunner 2049",
                     "Sci-fi detective movie",
                     "https://upload.wikimedia.org/wikipedia/en/9/9b/Blade_Runner_2049_poster.png",
                     "https://www.youtube.com/watch?v=dZOaI_Fn5o4")

# Create the website
movies = [toy_story, avatar, bladerunner2049]
freshTomatoes.open_movies_page(movies)

# Print the Class 'Movie' variable
print(media.Movie.VALID_RATINGS)

# Print the documentation associated with the Class ' Movie' in Module 'media'
print(media.Movie.__doc__)