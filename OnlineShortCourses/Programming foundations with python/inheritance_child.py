# -*- coding: utf-8 -*-
"""
Created on Sun Mar 24 18:19:15 2019

@author: jason
"""

import inheritance

#billy_cyrus = inheritance.Parent("Cyrus","blue")

miley_cyrus = inheritance.Child("Cyrus","Blue",5)

# Test
# print(miley_cyrus.last_name)
# print(miley_cyrus.number_of_toys)


miley_cyrus.show_info()