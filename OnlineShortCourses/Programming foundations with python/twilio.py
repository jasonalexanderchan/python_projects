# -*- coding: utf-8 -*-
"""
Created on Sat Mar 23 23:43:57 2019

@author: jason
"""
import twilio

# Download the helper library from https://www.twilio.com/docs/python/install
from twilio.rest import Client


# Your Account Sid and Auth Token from twilio.com/console
# DANGER! This is insecure. See http://twil.io/secure
account_sid = 'ACaee4e384c48b44e40500e5427854245e'
auth_token = ''  # Deleted to prevent security leak.
client = Client(account_sid, auth_token)

message = client.messages \
                .create(
                     body="Join Earth's mightiest heroes. Like Kevin Bacon.",
                     from_='+61429210735',
                     to='+61'  # Deleted to preven security leak
                 )

print(message.sid)