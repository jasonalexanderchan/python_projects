import turtle

def draw_square(someTurtle):
    for i in range(4):
        someTurtle.forward(100)
        someTurtle.right(90)

def draw_all():
    window = turtle.Screen()
    window.bgcolor("red")

    #Create turtle, Brad
    brad = turtle.Turtle()
    brad.shape("turtle"); brad.color("yellow"); brad.speed(2)
    draw_square(brad)

    #Create turtle, Sam
    sam = turtle.Turtle()
    sam.shape("turtle"); sam.color("blue"); sam.speed(2)
    sam.circle(100)

    #Create turtle, Chris
    chris = turtle.Turtle()
    chris.shape("turtle"); chris.color("green"); chris.speed(2)

    for i in range(2):
        chris.forward(100)
        chris.left(120)        
    chris.forward(100)
    
    window.exitonclick()

draw_all()
