This is painful to get working. Hopefully I don't need to try for 1hr for my next project. Follow these requirements!

1. All environments shall apply pytest. In settings type Python Configure Tests: Choose pytest.
2. All subdirectories shall have an __init__.py file e.g. src and a test folders need their own.
3. Tests shall be prefixed with 'test_<description>.py' because pytest looks for these!

See https://code.visualstudio.com/docs/python/testing