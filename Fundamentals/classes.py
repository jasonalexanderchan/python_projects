# -*- coding: utf-8 -*-
"""
Created on Sat Dec  4 12:37:57 2021

@author: jason
"""

# Enumerates
class MyEnum:
    Geeks, For, Geeks = range(3)
  
print(MyEnum.Geeks)
print(MyEnum.For)
print(MyEnum.Geeks)

variable = 4*MyEnum.Geeks
print(variable)



class MyClass(object):
    def __init__(self, number):
        self.number = number

my_objects = []

for i in range(100):
    my_objects.append(MyClass(i))

# later

for obj in my_objects:
    print(obj.number)