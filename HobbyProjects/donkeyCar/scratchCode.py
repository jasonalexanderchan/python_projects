# -*- coding: utf-8 -*-
"""
Created on Mon May 20 20:35:19 2019

@author: jason
"""
import shutil
import os
import re

# Assumptions
# .json filename has the same number in .jpg filename

# User input
path = 'C:\\Users\\jason\\OneDrive\\Desktop\\noiseTest\\'

# Copy jpg file and rename with _A
jpg_filename_full = '0_cam-image_array_.jpg'
jpg_filename_current = os.path.splitext(jpg_filename_full)[0]
jpg_filename_new = jpg_filename_current + 'A.jpg'
shutil.copy(path + jpg_filename_full, path + jpg_filename_new)

# Extract index number from jpg filename
idx = re.findall(r'\d+', jpg_filename_full) # type = list
idx_str = ''.join(idx)

# Create variable for matching .json filename
json_filename_full = 'record_' + idx_str + '.json'
json_filename_current = 'record_' + idx_str
json_filename_new = json_filename_current + '_A.json'

# Copy json file and rename with _A
shutil.copy(path + json_filename_full, path + json_filename_new)

# Open json file and replace jpg_filename_current with jpg_filename_new
json_contents_current = open(path + json_filename_new, "r").read() 
json_contents_new = json_contents_current.replace(jpg_filename_full, jpg_filename_new)
f = open(path + json_filename_new, "w")
f.write(json_contents_new)
f.close()


# Scratch code 
# Find .json file with that index number
# json_list = [f for f in os.listdir(path) if f.endswith('.json')] # type = list

