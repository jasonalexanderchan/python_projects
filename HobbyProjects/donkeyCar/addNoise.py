# -*- coding: utf-8 -*-
"""
Created on Mon May 20 23:26:46 2019

@author: jason
"""

import os
from scipy import ndarray
import shutil
import re
import random

# User input
path = 'C:\\Users\\jason\\OneDrive\\Desktop\\noiseTest\\'

# Get list of jpgs in path
jpg_list = [f for f in os.listdir(path) if f.endswith('.jpg')] # type = list

# image processing library
import skimage as sk
from skimage import transform
from skimage import util
from skimage import io

def random_rotation(image_array: ndarray):
    # pick a random degree of rotation between 25% on the left and 25% on the right
    random_degree = random.uniform(-25, 25)
    return sk.transform.rotate(image_array, random_degree)

def random_noise(image_array: ndarray):
    # add random noise to the image
    return sk.util.random_noise(image_array)

def horizontal_flip(image_array: ndarray):
    # horizontal flip doesn't need skimage, it's easy as flipping the image array of pixels !
    return image_array[:, ::-1]

# TO DO: Create options for user to specify which transformations they want to apply
# TO Do: Add if statement in loop to treat .json edits differently for horizontal_flip
# dictionary of the transformations. Usage available_transformations
# available_transformations = {
#    1: random_rotation,
#    2: random_noise,
#    3: horizontal_flip
# }

# Run loop on list of jpgs in path
for image in jpg_list:
    
    # Read image 
    image_to_transform = sk.io.imread(path + image)
    
    # Apply transformations
    transformed_image_noise = random_noise(image_to_transform)
    # transformed_image_rotation = random_noise(image_to_transform)

    # Copy jpg filename and rename as _Noise.jpg
    jpg_filename_current = os.path.splitext(image)[0]
    jpg_filename_noise = jpg_filename_current + 'noise.jpg'
    
    # Extract index number from jpg filename
    idx = re.findall(r'\d+', image) # type = list
    idx_str = ''.join(idx)
    
    # Write noise image to the disk
    jpg_filepath_noise = path + jpg_filename_noise
    io.imsave(jpg_filepath_noise, transformed_image_noise)

    # Create variable for the matching .json filename
    json_filename_full = 'record_' + idx_str + '.json'
    json_filename_current = 'record_' + idx_str
    json_filename_noise = json_filename_current + '_noise.json'

    # Copy json file and rename with new filename
    shutil.copy(path + json_filename_full, path + json_filename_noise)

    # Open json file and replace jpg_filename_current with jpg_filename_noise
    json_contents_current = open(path + json_filename_noise, "r").read() 
    json_contents_noise = json_contents_current.replace(image, jpg_filename_noise)
    f = open(path + json_filename_noise, "w")
    f.write(json_contents_noise)
    f.close()
