# -*- coding: utf-8 -*-
"""
Created on Sun Jul  7 16:56:28 2019

@author: jason
"""

import numpy as np
import cv2
from matplotlib import pyplot as plt

# Load an color image in grayscale
img = cv2.imread('C:\\Users\\jason\\OneDrive\\Desktop\\10346_cam-image_array_.jpg',0)

cv2.imshow('image',img)
cv2.waitKey(0)
cv2.destroyAllWindows()

plt.imshow(img, cmap = 'gray', interpolation = 'bicubic')
plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis
plt.show()
