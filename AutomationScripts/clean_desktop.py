# This script cleans up my desktop by creating a subfolder in delMe\\ on my desktop
# It then moves all other files and directories there

import os
from datetime import datetime
import argparse

# Parse arguments
parser = argparse.ArgumentParser(description='Moves all folders and files on your desktop into a folder: <folderName>/<todaysDate>')
parser.add_argument('mypath', type = str, help = 'Filepath of your desktop. String', nargs = '?', default = 'c://Users//jason//Desktop//')
parser.add_argument('folderName', type = str, help = 'Name of folder on your desktop. String', nargs = '?', default = 'delMe//')
args = parser.parse_args()

def clean_desktop(mypath, folderName):
    """Moves all folders and files on your desktop into a folder: <folderName>/<todaysDate>

    Args:
      mypath = filepath of your desktop
      folderName = the name of the parent folder on your desktop to store your files

    Returns:
      A subfolder named with today's date is created within folderName
      All files and folders on desktop are moved into the subfolder.

    Raises:
      ConnectionError: If no available port is found.
    """

    # Create variable for subfolder name with today's date
    today = datetime.now()
    year = str(today.year)
    month = str(today.month)
    day = str(today.day)

    if len(month) == 1:
        month = '0' + month

    if len(day) == 1:
        day = '0' + day

    today_string = year[-2:] + month + day

    # Check if folderName already exists in mypath. If false,  create folderName.
    # Check if target_folder already exists in folderName. If true, append number to variable today_string
    try:
        dirs_all = os.listdir(mypath + folderName)
    except:
        os.mkdir(mypath + folderName)
        dirs_all = os.listdir(mypath + folderName)

    dirs_same = [i for i in dirs_all if today_string in i]

    dirs_with_underscore = []
    for dir in dirs_same:
        if '_' in dir:
            dirs_with_underscore.append(dir)
    print('The number of subfolders created today : ' + str(dirs_with_underscore))

    if dirs_with_underscore == []:
        today_string = today_string + '_1//'
    else:
        number_max = 0
        for dir in dirs_with_underscore:
            number_next = int(dir[-1]) + 1
            if number_next > number_max:
                number_max = number_next
        today_string = today_string + '_' + str(number_max) + '//'

    # Create the subfolder in folderName
    os.mkdir(mypath + folderName + today_string)
    print('New subfolder name is: ' + today_string)
    print('The subfolder\'s path is: ' + mypath + folderName + today_string)

    # Move all desktop folders and files into the subfolder
    for item in os.listdir(mypath):
        if item in folderName or item == 'clean_desktop.bat':
            continue
        else:
            print("Moving ..." + item)
            os.rename(mypath + item, mypath + folderName + today_string + item)
    print('!!! DONE !!!')

if __name__ == '__main__':
    clean_desktop(args.mypath, args.folderName)
