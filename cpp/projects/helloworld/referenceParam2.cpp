#include <iostream>
#include <cmath>
using namespace std;

// reference parameter is root1 and root2. 
// recall that we'll assign root1 and root2 as the output of the quadratic function
void quadratic(int a, int b, int c, double& root1, double& root2){
    double d = sqrt(b*b - 4*a*c);
    root1 = (-b + d) / 2*a;
    root2 = (-b - d) / 2*a;
}

// declare double type for root1 and root2
int main(){
    double root1, root2;
    quadratic(3, 10, 1, root1, root2);
    cout << root1 << endl;
    cout << root2 << endl;
    return 0;
}