#include <iostream>
using namespace std;

// The functions must be declared first otherwise compile error.
void myfunc() {
    cout << "This is my function :)" << endl;
}

// cpp workaround is to declare an IOU (a function prototype)
void song(); // This is my function prototype
void printLine(int width, char letter); // This is my other function prototype

int main() {
    cout << "Hello, world!" << endl;
    myfunc();
    song();
    printLine(7, '?');
    return 0;
}

void song() {
    cout << "Hey everybody! This is my function prototype.";
}

void printLine(int width = 10, char letter = '*'){
    for (int i = 0; i < width; i++){
        cout << letter;
    }
}

// Reference Semantics vs. Value semantics (VS)
// 