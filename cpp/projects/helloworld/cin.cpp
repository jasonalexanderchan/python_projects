#include <iostream> // makes cout visible
using namespace std;

int main() {
    cout << "Hello, world!" << endl;

    int age;

    cout << "Please print your age"; // prompt
    cin >> age; // cin is discouraged because there's not data type checks
    cout << "You are " << age << " years old!" << endl;

    return 0;
}