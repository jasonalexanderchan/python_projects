#include <iostream>
using namespace std;

// comment: This is a really curious way to assign function outputs! Unlike matlab and python
void datingRange(int age, int& min, int& max){
    min = age / 2 + 7;
    max = (age - 7) * 2;
}

int main(){
    int young;
    int old;
    datingRange(48, young, old);
    cout << "A 48 year old could date someone from" << young << " to" << old << " years old." << endl;
}