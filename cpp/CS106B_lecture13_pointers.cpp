#include <iostream>

using namespace std;

// There are two ways to declare an variables/objects
// 1. Without pointers. But they get destroyed in local functions.l
// 2. With pointers. This is a feature = this is how we get persistent variables.

int main(){
    int  x = 42;
    int* p = &x; 

    // access the pointee
    cout << p << endl;  // get the memory address of variable x

    // *p means the 'value that p points to'. This is called dereferencing the pointer.
    cout << *p << endl; // get the data stored at the memory address stored in variable p

    // modify the pointee
    *p = 99; // change the data stored in the memory address stored in variable p
    cout << x << endl; // 99

    // Null pointers
    int* p1 = nullptr;   // stores 0
    int* p2;             // uninitialised
    // cout << *p1 << endl; // KABOOM. Segfault.
    // cout << *p2 << endl; // KABOOM. Segfault.

    // testing for nullness
    if (p1 == nullptr) {
        cout << "p1 is a null pointer"; // querying null pointers won't cause segfaults.
    }

}